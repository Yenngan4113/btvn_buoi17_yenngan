let numberArr = [];
document.querySelector("#add_number").addEventListener("click", function () {
  var numberValue = document.querySelector("#txt_number").value.trim() * 1;
  numberArr.push(numberValue);
  document.querySelector("#array").innerHTML = `Mảng hiện tại là: ${numberArr}`;
});

// Bài 1+2
document.querySelector("#bai_1").addEventListener("click", function () {
  var soDuongArr = [];
  var tongSoDuong = 0;
  numberArr.forEach(function (item) {
    if (item > 0) {
      soDuongArr.push(item);
    }
  });
  soDuongArr.forEach(function (item1) {
    tongSoDuong += item1;
  });
  document.querySelector(
    "#ket_qua_bai_1"
  ).innerHTML = /*html*/ `<div>Mảng là ${soDuongArr}</div>
  <div>Tông Số Dương là: ${tongSoDuong}</div>
  Tổng số lượng số dương: ${soDuongArr.length}`;
});
// Bài 3
document.getElementById("bai_3").addEventListener("click", function () {
  var [...minNumber] = numberArr;
  minNumber.sort(function (a, b) {
    return a - b;
  });
  var min = minNumber[0];
  document.querySelector("#ket_qua_bai_3").innerHTML = /*html*/ `
  Mảng là: ${minNumber} 
  <div>Số nhỏ nhất trong mảng là ${min}</div>`;
});
// Bài 4
document.querySelector("#bai_4").addEventListener("click", function () {
  soDuongArr = [];
  numberArr.forEach(function (item) {
    if (item > 0) {
      soDuongArr.push(item);
    }
  });
  soDuongArr.sort(function (a, b) {
    return b - a;
  });
  console.log(soDuongArr);
  var minSoDuong = soDuongArr[soDuongArr.length - 1];
  document.querySelector("#ket_qua_bai_4").innerHTML = /*html*/ `
  Mảng là: ${soDuongArr} 
  <div>Số nhỏ nhất trong mảng là ${minSoDuong}</div>`;
});
// Bài 5
document.querySelector("#bai_5").addEventListener("click", function () {
  var soChanArr = [];
  var lastSoChan = 0;
  numberArr.forEach(function (item) {
    if (item % 2 === 0) {
      soChanArr.push(item);
    }
  });
  if (soChanArr.length === 0) {
    lastSoChan = -1;
  } else {
    lastSoChan = soChanArr[soChanArr.length - 1];
  }
  document.querySelector("#ket_qua_bai_5").innerHTML = /*html*/ `
  Mảng số chẵn là: ${soChanArr} 
  <div>Số chẵn cuối cùng trong mảng là ${lastSoChan}</div>`;
});
// Bài 6
document.querySelector("#bai_6").addEventListener("click", function () {
  console.log(numberArr);
  let [...doiChoArr] = numberArr;
  let a = document.querySelector("#position_1").value.trim() * 1;
  let b = document.querySelector("#position_2").value.trim() * 1;
  let tmp = doiChoArr[a];
  doiChoArr[a] = doiChoArr[b];
  doiChoArr[b] = tmp;
  document.querySelector(
    "#ket_qua_bai_6"
  ).innerHTML = `Mảng sau khi thay đổi vị trí là ${doiChoArr}`;
});

// Bài 7
document.querySelector("#bai_7").addEventListener("click", function () {
  let [...numberAscending] = numberArr;
  numberAscending.sort(function (a, b) {
    return a - b;
  });
  document.querySelector(
    "#ket_qua_bai_7"
  ).innerHTML = ` Mảng theo thứ tự tăng dần là ${numberAscending}`;
});

// Bài 8
function isPrime(n) {
  let flag = 1;
  if (n < 2 || Math.ceil(n) != Math.floor(n)) {
    flag = 0;
  }
  for (var i = 2; i < n; i++) {
    if (n % i == 0) {
      flag = 0;
    }
  }
  return flag;
}

soNguyenTo = 0;
document.querySelector("#bai_8").addEventListener("click", function () {
  for (let i = 0; i < numberArr.length; i++) {
    if (isPrime(numberArr[i]) == 1) {
      soNguyenTo = numberArr[i];
      break;
    } else {
      soNguyenTo = -1;
    }
  }
  document.querySelector(
    "#ket_qua_bai_8"
  ).innerHTML = ` Số nguyên tố đầu tiên là ${soNguyenTo}`;
});

// Bài 9
function isSoNguyen(n) {
  let flag = 1;
  if (Math.ceil(n) != Math.floor(n)) {
    flag = 0;
  } else {
    return flag;
  }
}
document.querySelector("#bai_9").addEventListener("click", function () {
  let soNguyenArr = [];
  for (var index = 0; index < numberArr.length; index++) {
    if (isSoNguyen(numberArr[index]) == 1) {
      soNguyenArr.push(numberArr[index]);
    }
  }
  document.querySelector(
    "#ket_qua_bai_9"
  ).innerHTML = ` Mảng số nguyên là ${soNguyenArr} - số lượng số nguyên là ${soNguyenArr.length}`;
});
// Bài 10
document.querySelector("#bai_10").addEventListener("click", function () {
  let soDuongArr = [];
  let soAmArr = [];
  for (let index = 0; index < numberArr.length; index++) {
    let item = numberArr[index];
    if (item >= 0) {
      soDuongArr.push(item);
    } else {
      soAmArr.push(item);
    }
    console.log(soDuongArr.length, soAmArr.length);
  }

  if (soDuongArr.length == soAmArr.length) {
    document.querySelector(
      "#ket_qua_bai_10"
    ).innerHTML = ` <div>Mảng số dương là ${soDuongArr}</div>
    <div>Mảng số âm là ${soAmArr}</div>
    <div>Số Dương = Số Âm`;
  } else if (soDuongArr.length > soAmArr.length) {
    document.querySelector(
      "#ket_qua_bai_10"
    ).innerHTML = ` <div>Mảng số dương là ${soDuongArr}</div>
    <div>Mảng số âm là ${soAmArr}</div>
    <div>Số Dương > Số Âm`;
  } else {
    document.querySelector(
      "#ket_qua_bai_10"
    ).innerHTML = ` <div>Mảng số dương là ${soDuongArr}</div>
    <div>Mảng số âm là ${soAmArr}</div>
    <div>Số Dương < Số Âm`;
  }
});
